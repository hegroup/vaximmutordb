<?php 
include_once('../inc/functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
  <?php 
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);

$t_table_def = get_table_def();
$t_gene_def = $t_table_def['t_gene'];


$vali=new Validation($_REQUEST);
$c_gene_id = $vali->getNumber('c_gene_id', 'Gene ID', 1, 10);

if ($vali->getErrorMsg()=='') { 
	
	$strSql = "SELECT c_vaccine_id, c_vaccine_name FROM t_vaccine WHERE ((c_vaccine_id IN (SELECT c_vaccine_id from t_host_response where c_host_response_id IN (SELECT c_host_response_id from t_host_gene_response where c_gene_id = $c_gene_id)) OR c_vaccine_id IN (SELECT c_vaccine_id from t_gene_engineering where c_gene_id = $c_gene_id))) AND c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated) order by c_vaccine_name";
	$rs = $db->Execute($strSql);
	$array_gene_vaccine = array();
	if (!$rs->EOF){
		$array_gene_vaccine = $rs->GetArray();
	}
	
	$strSql = "select * from t_gene where c_gene_id=$c_gene_id AND c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
	
	$rs = $db->Execute($strSql);
	if (!$rs->EOF)
	{
		$array_gene = $rs->GetArray();
		$gene = $array_gene[0];
?>
  <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
    <tr align="center">
      <td colspan="2"><h3>
          <?php echo formatOutput($gene['c_gene_name'])?>
        </h3></td>
    </tr>
    <?php 
	foreach ($t_gene_def as $gene_column) {
		if ($gene_column['c_column_in_details']==1 && $gene[$gene_column['c_column_name']]!='') {
?>
    <tr>
      <td width="120" bgcolor="#E4E4E4"><span class="styleLeftColumn">
        <?php echo $gene_column['c_column_label']?>
        </span> </td>
      <td bgcolor="#F4F9FD"><span class="smallContent">
        <?php 
			if ($gene_column['c_column_url']!='') {
				
?>
<a href="<?php echo $gene_column['c_column_url']?><?php echo $gene[$gene_column['c_column_name']]?>" target="_blank"><?php echo $gene[$gene_column['c_column_name']]?></a>
        <?php 
			}
			elseif ($gene_column['c_column_content']=='fasta') {
?>
            <pre style="font-size:12px "><?php echo $gene[$gene_column['c_column_name']]?></pre>
          <form action="../blast/blast_cs.php" method="post">
            <input name="sequence" type="hidden" value="<?php echo $gene[$gene_column['c_column_name']]?>" />
                <input name="BLAST" type="submit" value="  BLAST  " />
          </form>
			
        <?php 
			}
			else {
?>
          <?php echo formatOutput($gene[$gene_column['c_column_name']])?>
        <?php 
			}
?>
      </span> </td>
    </tr>
    <?php 
		}
	}
	if (sizeof($array_gene_vaccine) >0) {
?>
    <tr>
      <td bgcolor="#E4E4E4"><span class="styleLeftColumn">Related Vaccines(s)</span> </td>
      <td bgcolor="#F4F9FD"><span class="smallContent">
        <?php 
		$i=0;
		foreach ($array_gene_vaccine as $gene_vaccine) {
			if ($i>0) {
?>
        ,
        <?php 									
			}
?>
        <a href="../vaxquery/vaccine_detail.php?c_vaccine_id=<?php echo $gene_vaccine['c_vaccine_id']?>">
        <?php echo $gene_vaccine['c_vaccine_name']?>
        </a>
        <?php 
			$i++;
		}
?>
        </span> </td>
    </tr>
    <?php 
	}
?>
	<tr bgcolor="#2A4184">
	  <td bgcolor="#E4E4E4"><span class="styleLeftColumn">References </span> </td>
		<td bgcolor="#F4F9FD" style=" font-size:12px"><?php 
	$strSql = "SELECT * FROM t_reference WHERE c_gene_id = $c_gene_id AND c_curation_flag IN ($curation_flag_reviewed,$curation_flag_updated) ORDER BY c_reference_name";
	
	//echo $strSql;
	$rs = $db->Execute($strSql);
	if (!$rs->EOF){
		$array_reference = $rs->GetArray();
		$rs->Close();
?>
                        <?php 
		foreach ($array_reference as $reference) {
?>
                        <blockquote class="block_references"  style="word-break: break-all;" id="reference<?php echo $reference['c_reference_id']?>"><?php  echo getRef($reference)?></blockquote>
                        <?php 
		}
?>
                      <?php 

	}
	else {
		echo '&nbsp;';
	}
?>
	  </td>
    </tr>
  </table>
<br />
<br />
  <?php 
		$rs->Close();
	}
	else {
?>
  <p align="center">&nbsp; </p>
  <p align="center">No gene was found. Please use different keywords. </p>
  <?php 

	}
}
else {
?>
                        <p align="center">&nbsp; </p>
                        <p align="center">Something wrong with the gene ID. <a href="../index.php">Go back and try again</a>. </p>
                        <?php 

}
?>
  <!-- InstanceEndEditable -->
</div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
