<?php 
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
  <h3>Vaximmutordb Statistics</h3>
  <?php 

$t_table_def = get_table_def();
$t_gene_def = $t_table_def['t_gene'];


$array_gene_vaccine = array();

$strSql = "SELECT c_vaccine_name, t_vaccine.c_vaccine_id, c_gene_id FROM t_host_response join t_vaccine on t_host_response.c_vaccine_id=t_vaccine.c_vaccine_id join t_host_gene_response on t_host_gene_response.c_host_response_id = t_host_response.c_host_response_id WHERE t_host_response.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";

$rs = $db->Execute($strSql);
if (!$rs->EOF)
{
	$array_vaccine = $rs->GetArray();
	$rs->Close();
	
	foreach ($array_vaccine as $vaccine) {
		if (!array_key_exists($vaccine['c_gene_id'], $array_gene_vaccine)) {
			$array_gene_vaccine[$vaccine['c_gene_id']] = array();
		}
		$array_gene_vaccine[$vaccine['c_gene_id']][] = array($vaccine['c_vaccine_id'], $vaccine['c_vaccine_name']);
	}
}

$strSql = "SELECT distinct t_gene.* FROM t_pathogen";
$strSql .= " join t_host_gene_response on t_host_gene_response.c_pathogen_id=t_pathogen.c_pathogen_id";
$strSql .= " join t_gene on t_host_gene_response.c_gene_id=t_gene.c_gene_id";
$strSql .= " where (c_phi_function='Vaximmutor' OR c_phi_function2='Vaximmutor')";
$strSql .= " and t_pathogen.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " and t_host_gene_response.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " and t_gene.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";

$array_gene = array();

$rs = $db->Execute($strSql);

//error_log($strSql);

if (!$rs->EOF)
{
	$array_gene = $rs->GetArray();
	$rs->Close();
?>
  <p> Vaximmutordb now has <?php echo sizeof($array_gene)?> vaximmutors.
    <?php 
	
	$cog_cat_table=array();
	$strSql = "SELECT * from pathinfo.cog_cat order by cog_cat_label";
	$rs = $db->Execute($strSql);
	foreach ($rs as $row) {
		$cog_cat_table[$row['cog_cat_id']] = $row['cog_cat_label'];
	}
	
	$params="c_pathogen_id=$c_pathogen_id&c_phi_function=$c_phi_function&c_gene_name=$c_gene_name&c_gene_locus_tag=$c_gene_locus_tag";
	$params.="&db_type=$db_type&db_id=$db_id&c_max_tmhmm_PredHel=$c_max_tmhmm_PredHel&c_max_tmhmm_PredHel_check=$c_max_tmhmm_PredHel_check";
	$params.="&c_min_spaan_score_check=$c_min_spaan_score_check&c_min_spaan_score=$c_min_spaan_score&keywords=$keywords";
	$params.="&c_human_alignment=$c_human_alignment&c_mouse_alignment=$c_mouse_alignment";
	
	foreach ($array_c_localization as $tmp_localization) {
		$params.="&c_localization[]=$tmp_localization";
	}
	
	foreach ($array_cog_cat_id as $tmp_cog_cat_id) {
		$params.="&cog_cat_id[]=$tmp_cog_cat_id";
	}
?>
  </p>
  <table border="0" cellpadding="2" cellspacing="2">
    <tr>
      <td height="25" align="center" bgcolor="#A5C3D6" class="styleLeftColumn">Vaximmutordb ID</td>
      <?php 
	foreach ($t_gene_def as $gene_column) {
		if ($gene_column['c_column_in_results']==1) {
			if ($gene_column['c_column_name']=='c_taxonomy_id') {
?>
      <td align="center" bgcolor="#A5C3D6" class="styleLeftColumn">Pathogen Name</td>
      <?php 
			}
?>
      <td align="center" bgcolor="#A5C3D6" class="styleLeftColumn"><?php echo $gene_column['c_column_label']?></td>
      <?php 
		}
	}
?>
      <td height="25" align="center" bgcolor="#A5C3D6" class="styleLeftColumn">Vaccines Involving this Gene </td>
    </tr>
    <?php 
	foreach ($array_gene as $gene) {
?>
    <tr>
      <td bgcolor="#F5FAF7" class="smallContent"><b><a href="gene_detail.php?c_gene_id=<?php echo $gene['c_gene_id']?>"><?php echo formatOutput($gene['c_gene_id'])?></a></b></td>
      <?php 
		foreach ($t_gene_def as $gene_column) {
			if ($gene_column['c_column_in_results']==1) {
				if ($gene_column['c_column_name']=='c_taxonomy_id') {
?>
      <td bgcolor="#F5FAF7" class="smallContent"><?php echo formatOutput($gene['c_pathogen_name'])?></td>
      <?php 
				}
?>
      <td bgcolor="#F5FAF7" class="smallContent"><?php 
				if ($gene_column['c_column_url']!='') {
					
?>
        <a href="<?php echo $gene_column['c_column_url']?><?php echo $gene[$gene_column['c_column_name']]?>" target="_blank"><?php echo $gene[$gene_column['c_column_name']]?></a>
        <?php 
				}
				else {
?>
        <?php echo formatOutput($gene[$gene_column['c_column_name']])?>
        <?php 
			}
?></td>
      <?php 
		}
	}
?>
      <td bgcolor="#F5FAF7" class="smallContent"><?php  echo array_key_exists($gene['c_gene_id'], $array_gene_vaccine) ? sizeof($array_gene_vaccine[$gene['c_gene_id']]) : 0 ?></td>
    </tr>
    <?php 
		}
?>
  </table>
  <?php 
}
?>
  <!-- InstanceEndEditable --></div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
