<?php 
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
<h3>Vaximmutordb Search</h3>

  <?php 
$vali=new Validation($_REQUEST);


$highLightStr = '';
$keywords = $vali->getInput('keywords', 'Keywords', 0, 60);
$c_pathogen_id = $vali->getNumber('c_pathogen_id', 'Pathogen', 0, 10);
$c_host_id = $vali->getNumber('c_host_id', 'Host', 0, 10);
$c_vaccine_name = $vali->getInput('c_vaccine_name', 'Vaccine Name', 0, 60);
$c_gene_name = $vali->getInput('c_gene_name', 'Gene Name', 0, 60);
$c_phi_function = $vali->getInput('c_phi_function', 'Pathogen-Host Interaction', 0, 30);
$c_gene_locus_tag = $vali->getInput('c_gene_locus_tag', 'Locus Tag', 0, 60);
$c_gene_name = $vali->getInput('c_gene_name', 'Locus Name', 0, 60);

$db_type = $vali->getInput('db_type', 'Database Type', 0, 60);
$db_id = $vali->getInput('db_id', 'Database ID', 0, 60);


$c_gene_use = $vali->getInput('c_gene_use', 'Gene Type', 0, 10);
$array_cog_cat_id = $vali->getArray('cog_cat_id', 'COG Category', false);
$array_cog_cat_id = $array_cog_cat_id=='' ? array() : $array_cog_cat_id;

$tkeywords = transformKeywords($keywords);

$array_c_localization = $vali->getArray('c_localization', 'subcellular localization', false);
$c_min_spaan_score = $vali->getNumber('c_min_spaan_score', 'Minimum SPAAN score', 0, 4);
$c_max_tmhmm_PredHel = $vali->getNumber('c_max_tmhmm_PredHel', 'Maximum TMHMM predicted transmembrane helices', 0, 4);
$c_max_tmhmm_PredHel_check = $vali->getInput('c_max_tmhmm_PredHel_check', 'Include "maximum number of transmembrane helices"', 0, 4);
$c_min_spaan_score_check = $vali->getInput('c_min_spaan_score_check', 'Include "Minimum SPAAN score"', 0, 4);
$c_hit_p_value = $vali->getInput('c_hit_p_value', 'Maximum P value of sequence similarity to known epitopes', 0, 10);
$c_human_alignment = $vali->getInput('c_human_alignment', 'Include "No similarity to human proteins"', 0, 4);
$c_mouse_alignment = $vali->getInput('c_mouse_alignment', 'Include "No similarity to mouse proteins"', 0, 4);
$array_c_localization = $array_c_localization=='' ? array() : $array_c_localization;


$t_table_def = get_table_def();
$t_gene_def = $t_table_def['t_gene'];


$array_gene_vaccine = array();

// 2017/06/11 Edison added vaccine query
$strSql = "SELECT c_vaccine_name, t_vaccine.c_vaccine_id, c_gene_id FROM t_host_response join t_vaccine on t_host_response.c_vaccine_id=t_vaccine.c_vaccine_id join t_host_gene_response on t_host_gene_response.c_host_response_id = t_host_response.c_host_response_id WHERE";
$strSql .= " c_vaccine_name like '%$c_vaccine_name%'";
$strSql .= " AND t_host_response.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";

$rs = $db->Execute($strSql);
if (!$rs->EOF)
{
	$array_vaccine = $rs->GetArray();
	$rs->Close();
	
	foreach ($array_vaccine as $vaccine) {
		if (!array_key_exists($vaccine['c_gene_id'], $array_gene_vaccine)) {
			$array_gene_vaccine[$vaccine['c_gene_id']] = array();
		}
		$array_gene_vaccine[$vaccine['c_gene_id']][] = array($vaccine['c_vaccine_id'], $vaccine['c_vaccine_name']);
	}
}

$strSql = "SELECT distinct t_gene.* FROM t_pathogen";
$strSql .= " join t_host_gene_response on t_host_gene_response.c_pathogen_id=t_pathogen.c_pathogen_id";
$strSql .= " join t_gene on t_host_gene_response.c_gene_id=t_gene.c_gene_id";
// 2017/06/11 Edison added host query
$strSql .= " JOIN t_host_response ON t_host_response.c_host_response_id=t_host_gene_response.c_host_response_id";
$strSql .= " JOIN t_host ON t_host.c_host_id = t_host_response.c_host_id";
$strSql .= " where (c_phi_function='Vaximmutor' OR c_phi_function2='Vaximmutor')";
$strSql .= " and t_pathogen.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " and t_host_gene_response.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " and t_gene.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
// 2017/06/11 Edison added host curation flag
$strSql .= " and t_host.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";



if ($c_pathogen_id!='') {
	$strSql .= " AND t_pathogen.c_pathogen_id='$c_pathogen_id'";
}
// 2017/06/11 Edison added host filter
if ($c_host_id!='') {
	$strSql .= " AND t_host.c_host_id='$c_host_id'";
}
if ($c_gene_name!='') {
	$strSql .= " and c_gene_name like '%$c_gene_name%'";
}
if ($c_gene_locus_tag!='') {
	$strSql .= " and c_gene_locus_tag like '%$c_gene_locus_tag%'";
}

if ($db_type=='protegen_id'&& $db_id!='') {
	$strSql .= " AND t_gene.c_gene_id='$db_id'";
}

if ($db_type=='vo_id' && $db_id!='') {
	$strSql .= " AND t_gene.c_vo_id='$db_id'";
}

if ($db_type=='gene_gi' && $db_id!='') {
	$strSql .= " and c_ncbi_gene_id = '$db_id'";
}

if ($db_type=='nucleotide_gi' && $db_id!='') {
	$strSql .= " and c_ncbi_nucleotide_id = '$db_id'";
}

if ($db_type=='protein_acc' && $db_id!='') {
	$strSql .= " and c_protein_refseq = '$db_id'";
}

if ($db_type=='protein_gi' && $db_id!='') {
	$strSql .= " and c_ncbi_protein_id = '$db_id'";
}

if ($db_type=='other_id' && $db_id!='') {
	$strSql .= " and c_xrefs like '%$db_id%'";
}

if (!empty($array_cog_cat_id) && !in_array('', $array_cog_cat_id)) {
	$strSql .= " AND (1=2";
	foreach ($array_cog_cat_id as $cog_cat_id) {
		$strSql .= " OR c_cog_cat like '%$cog_cat_id%'";
	}
	$strSql .= ")";
}





if ($keywords!='') {
	$tkeywords = transformKeywords($keywords);
	$strSql .= " and match (c_gene_name, c_ncbi_gene_id, c_ncbi_nucleotide_id, c_xrefs, c_phi_annotation, c_strain, c_taxonomy_id, c_ncbi_protein_id, c_protein_refseq, c_protein_name, c_gene_locus_tag, c_gene_refseq, c_protein_note, c_protein_annotation, c_phi_function, c_phi_function2) against ('$tkeywords' in boolean mode)";
}


	
	$strSql2='';
	if (!empty($array_c_localization) && !in_array('Any', $array_c_localization)) {
		$strSql2 .= " AND (c_Final_Localization like '" . join("%' OR c_Final_Localization like '", $array_c_localization) . "%')";
	}
	if ($c_min_spaan_score != '' && $c_min_spaan_score_check=='true') {
		$strSql2 .= " AND c_SPAAN >= $c_min_spaan_score";
	}
	if ($c_max_tmhmm_PredHel != '' && $c_max_tmhmm_PredHel_check=='true') {
		$strSql2 .= " AND c_tmhmm_PredHel <= $c_max_tmhmm_PredHel";
	}
	
	if ($c_human_alignment=='true') {
		$strSql2 .= " AND NOT c_sequence_id in (select distinct (c_sequence_id) from t_vaxign_blast_results where c_query_id ='$c_query_id' and c_target_species='Human')";
	}
	if ($c_mouse_alignment=='true') {
		$strSql2 .= " AND NOT c_sequence_id in (select distinct (c_sequence_id) from t_vaxign_blast_results where c_query_id ='$c_query_id' and c_target_species='Mouse')";
	}
	
	if ($strSql2!='') {
		$c_query_ids = array();
		$strSql3= "select c_query_id from t_vaxign_query where c_note='t_gene'";
		$rs = $db->Execute($strSql3);
		foreach($rs as $row) {
			$c_query_ids[] = $row['c_query_id'];
		}
		
		$strSql2 = "SELECT c_sequence_acc FROM t_vaxign_analysis_results WHERE c_query_id in ('".join("','", $c_query_ids)."') " . $strSql2;
		
		$c_gene_ids = array();
		$rs = $db->Execute($strSql2);
		foreach($rs as $row) {
			$c_gene_ids[] = $row['c_sequence_acc'];
		}
		
		$strSql .= " AND c_gene_id in ('".join("','", $c_gene_ids)."')";
	}




$array_gene = array();

$rs = $db->Execute($strSql);

//error_log($strSql);

if (!$rs->EOF)
{
	$array_gene = $rs->GetArray();
	$rs->Close();
	/* 2017/06/12 Edison added REACTOME API analysis and link */
	$reactome_genes = array();
	foreach ($array_gene as $gene) {
		$reactome_genes[] = $gene['c_ncbi_gene_id'];
	}
	$url = "http://www.reactome.org/AnalysisService/identifiers/projection";
	$header = array( 'Content-Type: text/plain' );
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, "#Genes\n" . implode( "\n",$reactome_genes ) );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	$result = curl_exec( $ch );
	$json = json_decode( $result, true );
	$reactome_token = $json['summary']['token'];
	curl_close( $ch );
	/* End */
?>

                        <p> Found <?php echo sizeof($array_gene)?>
                         gene(s).
                         <a target="_blank" href="<?php echo "http://www.reactome.org/PathwayBrowser/#DTAB=AN&ANALYSIS=$reactome_token"?>">View enrichment analysis in REACTOME</a>
<?php 
	
	$cog_cat_table=array();
	$strSql = "SELECT * from pathinfo.cog_cat order by cog_cat_label";
	$rs = $db->Execute($strSql);
	foreach ($rs as $row) {
		$cog_cat_table[$row['cog_cat_id']] = $row['cog_cat_label'];
	}
	
	$params="c_pathogen_id=$c_pathogen_id&c_phi_function=$c_phi_function&c_gene_name=$c_gene_name&c_gene_locus_tag=$c_gene_locus_tag";
	$params.="&db_type=$db_type&db_id=$db_id&c_max_tmhmm_PredHel=$c_max_tmhmm_PredHel&c_max_tmhmm_PredHel_check=$c_max_tmhmm_PredHel_check";
	$params.="&c_min_spaan_score_check=$c_min_spaan_score_check&c_min_spaan_score=$c_min_spaan_score&keywords=$keywords";
	$params.="&c_human_alignment=$c_human_alignment&c_mouse_alignment=$c_mouse_alignment";
	
	foreach ($array_c_localization as $tmp_localization) {
		$params.="&c_localization[]=$tmp_localization";
	}
	
	foreach ($array_cog_cat_id as $tmp_cog_cat_id) {
		$params.="&cog_cat_id[]=$tmp_cog_cat_id";
	}
?>	
			    </p>
<table border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td height="25" align="center" bgcolor="#A5C3D6" class="styleLeftColumn">Vaximmutordb ID</td>
<?php 
	foreach ($t_gene_def as $gene_column) {
		if ($gene_column['c_column_in_results']==1) {
			if ($gene_column['c_column_name']=='c_taxonomy_id') {
?>
                            <td align="center" bgcolor="#A5C3D6" class="styleLeftColumn">Pathogen Name</td>
<?php 
			}
?>
                            <td align="center" bgcolor="#A5C3D6" class="styleLeftColumn"><?php echo $gene_column['c_column_label']?></td>
<?php 
		}
	}
?>
                            <td height="25" align="center" bgcolor="#A5C3D6" class="styleLeftColumn">COG</td>
                            <td height="25" align="center" bgcolor="#A5C3D6" class="styleLeftColumn">Vaccines Involving this Gene </td>
                          </tr>
<?php 
	foreach ($array_gene as $gene) {
		/* 2017/06/11 Edison filter vaccine result */
		if (!array_key_exists($gene['c_gene_id'], $array_gene_vaccine) || empty($array_gene_vaccine[$gene['c_gene_id']])) continue;
		/* End */
?>
                          <tr>
                            <td bgcolor="#F5FAF7" class="smallContent">
							<b><a href="gene_detail.php?c_gene_id=<?php echo $gene['c_gene_id']?>"><?php echo formatOutput($gene['c_gene_id'])?></a></b>
							</td>
<?php 
		foreach ($t_gene_def as $gene_column) {
			if ($gene_column['c_column_in_results']==1) {
				if ($gene_column['c_column_name']=='c_taxonomy_id') {
?>
                            <td bgcolor="#F5FAF7" class="smallContent"><?php echo formatOutput($gene['c_pathogen_name'])?></td>
<?php 
				}
?>
                            <td bgcolor="#F5FAF7" class="smallContent">
<?php 
				if ($gene_column['c_column_url']!='') {
					
?>
							<a href="../vaximmutordb/<?php echo $gene_column['c_column_url']?><?php echo $gene[$gene_column['c_column_name']]?>" target="_blank"><?php echo $gene[$gene_column['c_column_name']]?></a>
<?php 
				}
				else {
?>
							<?php echo formatOutput($gene[$gene_column['c_column_name']])?>
<?php 
			}
?>
							</td>
<?php 
		}
	}
?>

        	<td bgcolor="#F5FAF7" class="tdData"><?php echo $gene['c_cog']?>
<?php 
	$cogcat_labels = array();
	if (preg_match('/^COG\d+([A-Z]+)$/', $gene['c_cog'], $match)) {
		$cogcats = str_split($match[1]);
		
		foreach ($cogcats as $cogcat) {
			$cogcat_labels[] = $cogcat . ': ' . $cog_cat_table[$cogcat];
		}
?>
, under <?php echo join('; ', $cogcat_labels)?>
<?php 
}
?>

            </td>
                            <td bgcolor="#F5FAF7" class="smallContent">
<?php 
	if (array_key_exists($gene['c_gene_id'], $array_gene_vaccine)) {
		foreach ($array_gene_vaccine[$gene['c_gene_id']] as $gene_vaccine) {
?>
<li style="margin-left:14px; padding-left:0px;"><a href="../vaxquery/vaccine_detail.php?c_vaccine_id=<?php echo $gene_vaccine[0]?>"><?php echo $gene_vaccine[1]?></a></li>
<?php 			
		}
	}
?>
							</td>
                          </tr>
<?php 
		}
?>
</table>
<?php 
}
else {
?>
                        <p align="center">&nbsp; </p>
                        <p align="center">No gene was found. Please use different keywords. </p>
                        <p align="center">Get search results from <a href="http://www.phidias.us/phigen/index.php">PHIDIAS Phigen</a>.</p>
						<?php 
	if ($keywords!='') {
?>
<p align="center">Search "<?php echo $keywords?>" in 
<a href="http://www.violinet.org/vaxquery/query_process.php?adsearch=1&amp;column=any&amp;operation1=AND&amp;keywords1=&amp;column1=pathogen%3Ac_pathogen_name&amp;operation2=AND&amp;keywords2=&amp;column2=pathogen%3Ac_pathogen_name&amp;searchEngine=vaxquery&amp;keywords=<?php echo $keywords?>">Vaxquery</a>, 
<a href="http://www.violinet.org/litesearch/keywords_search_process.php?t1=c_fulltext&m1=contains&t2=c_title&m2=contains&q2=&t3=c_journal_book_name&m3=contains&q3=&year_from=any&year_to=present&volume=&issue=&page_start=&sort_by=title&order=desc&searchEngine=litesearch&q1=<?php echo $keywords?>">Litesearch</a>, 
<a href="http://www.violinet.org/textpresso/cgi-bin/search?species=All&cat1=none&cat2=none&cat3=none&cat4=none&literature=curated literature&target=abstract&target=body&target=title&sentencerange=sentence&mode=boolean&sort=score+%28hits%29&search=Search%21&.cgifields=casesensitive&.cgifields=exactmatch&searchEngine=vaxpresso&searchstring=<?php echo $keywords?>">Vaxpresso</a>, 
<a href="http://www.violinet.org/litesearch/meshtree/meshtree.php?searchEngine=vaxmesh&keywords=<?php echo $keywords?>">Vaxmesh</a></p>
<?php 
	}
}
?>
<!-- InstanceEndEditable -->
</div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
