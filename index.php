<?php 
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);

$total_antigen = 0;

/* Edison: Old SQL script does not take t_host_gene_response and t_pathogen into account
$strSql = "SELECT count(distinct c_gene_id) AS num_antigen FROM t_gene";
$strSql .= " where (c_phi_function='Vaximmutor' OR c_phi_function2='Vaximmutor')";
$strSql .= " and t_gene.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
*/
$strSql =
<<<END
SELECT COUNT(DISTINCT t_gene.c_gene_id) AS num_antigen
FROM 
t_pathogen 
JOIN t_host_gene_response ON t_host_gene_response.c_pathogen_id=t_pathogen.c_pathogen_id 
JOIN t_gene ON t_host_gene_response.c_gene_id=t_gene.c_gene_id 
WHERE
	(c_phi_function='Vaximmutor' OR c_phi_function2='Vaximmutor')
	and 
	t_pathogen.c_curation_flag in (10,2)
	and
	t_host_gene_response.c_curation_flag in (10,2)
	and
	t_gene.c_curation_flag in (10,2)
;
END;

$rs = $db->Execute($strSql);
if (!$rs->EOF)
{
	$total_antigen = $rs->fields['num_antigen'];
	$rs->Close();
}

$strSql = "SELECT distinct c_pathogen_name, t_pathogen.c_pathogen_id FROM t_pathogen";
$strSql .= " join t_host_gene_response on t_host_gene_response.c_pathogen_id=t_pathogen.c_pathogen_id";
$strSql .= " join t_gene on t_host_gene_response.c_gene_id=t_gene.c_gene_id";
$strSql .= " where (c_phi_function='Vaximmutor' OR c_phi_function2='Vaximmutor')";
$strSql .= " and t_pathogen.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " and t_host_gene_response.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " and t_gene.c_curation_flag in ($curation_flag_reviewed,$curation_flag_updated)";
$strSql .= " ORDER BY c_pathogen_name";


$array_pathogen = array();
$rs = $db->Execute($strSql);
if (!$rs->EOF)
{
	$array_pathogen = $rs->GetArray();
	$rs->Close();
}

/* 2017/06/11 Edison add host query */
$strSql =
<<<END
SELECT DISTINCT t_host.c_host_id, t_host.c_host_name FROM t_host
JOIN t_host_response ON t_host_response.c_host_id=t_host.c_host_id
JOIN t_host_gene_response ON t_host_gene_response.c_host_response_id=t_host_response.c_host_response_id
JOIN t_gene ON t_host_gene_response.c_gene_id=t_gene.c_gene_id
WHERE
(c_phi_function='Vaximmutor' OR c_phi_function2='Vaximmutor')
and
t_host.c_curation_flag in (10,2)
and
t_host_gene_response.c_curation_flag in (10,2)
and
t_gene.c_curation_flag in (10,2)
;
END;
$array_host = array();
$rs = $db->Execute($strSql);
if (!$rs->EOF)
{
	$array_host = $rs->GetArray();
	$rs->Close();
}
/* End */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
<!--
.style2 {font-size: 15px}
-->
</style>
<!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
<?php 
$cog_cats=array();
$strSql = "SELECT * from pathinfo.cog_cat order by cog_cat_label";
$rs = $db->Execute($strSql);
foreach ($rs as $row) {
	$cog_cats[$row['cog_cat_id']] = $row['cog_cat_label'];
}
?>

				<h3 align="center">VaximmutorDB: Vaccine Immune Effectors</h3>                
                
				<p>&nbsp;&nbsp;&nbsp;&nbsp; A &quot;vaximmutor&quot; is coined here to represent an immune effectors that is triggered by a vaccination and plays a role in vaccine-induced host immunity. Vaximmuntors are specifically targeted by the innate or acquired immune system  of the host and are able to induce acquired immunity and protection in the host against infectious and non-infectious diseases. Vaximmutors play important roles in development of vaccines and biological markers for disease diagnosis and analysis of fundamental host immunity against diseases. VaximmutorDB is a web-based central database and analysis system that curates, stores, and analyzes various vaccine-induced immune factors (i.e., Vaximmutors). </p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp; It is noted that this is still a new project in VIOLIN. More research is under way. Stay tuned ... </p>
				<form action="search_process.php" method="post" name="form_basic_search" id="form_basic_search">
					<table style="border:1px solid #999966; margin-left:20px;">
						<tr>
							<td height="40" colspan="2" bgcolor="#DDDDDD" ><strong> Vaximmutordb  Search </strong></td>
						</tr>
						<tr align="center" bgcolor="#ECEFF0">
							<td height="30" class="styleLeftColumn"><strong>Search Field</strong></td>
							<td bgcolor="#ECEFF0" class="styleLeftColumn"><strong>Search Parameter</strong></td>
						</tr>
						<tr>
							<td bgcolor="#F8FAFA" class="styleLeftColumn"><strong>Pathogen or Disease</strong></td>
							<td bgcolor="#F8FAFA" class="smallContent"><select name="c_pathogen_id" id="c_pathogen_id">
									<option value="">Any pathogen</option>
									<?php 
foreach ($array_pathogen as $pathogen) {
?>
									<option value="<?php echo $pathogen['c_pathogen_id']?>">
									<?php echo $pathogen["c_pathogen_name"]?>									</option>
									<?php 
}
?>
								</select>
							  <input type="hidden" name="c_phi_function" id="c_phi_function" value="Vaximmutor" /></td>
						</tr>
						<tr>
							<td bgcolor="#F8FAFA" class="styleLeftColumn"><strong>Host</strong></td>
							<td bgcolor="#F8FAFA" class="smallContent"><select name="c_host_id" id="c_host_id">
									<option value="">Any host</option>
									<?php 
foreach ($array_host as $host) {
?>
									<option value="<?php echo $host['c_host_id']?>">
									<?php echo $host["c_host_name"]?>									</option>
									<?php 
}
?>
								</select>
							  <input type="hidden" name="c_phi_function" id="c_phi_function" value="Vaximmutor" /></td>
						</tr>
						<tr>
							<td bgcolor="#F8FAFA" class="styleLeftColumn"><strong>Vaccine Name </strong></td>
							<td bgcolor="#F8FAFA" class="smallContent"><input name="c_vaccine_name" type="text" id="c_vaccine_name" size="40" maxlength="60" />
								(e.g., FluMist)</td>
						</tr>
						<tr>
							<td bgcolor="#F8FAFA" class="styleLeftColumn"><strong>Gene Name </strong></td>
							<td bgcolor="#F8FAFA" class="smallContent"><input name="c_gene_name" type="text" id="c_gene_name" size="40" maxlength="60" />
								(e.g., sodC)</td>
						</tr>
						<tr>
							<td class="styleLeftColumn"><strong>Locus Tag </strong></td>
							<td class="smallContent"><input name="c_gene_locus_tag" type="text" id="locus_tag" size="40" maxlength="60" />
								(e.g., BAB2_0535) </td>
						</tr>
						<tr>
						  <td bgcolor="#F8FAFA" class="styleLeftColumn">Database ID</td>
						  <td class="tdData"><select name="db_type" >
						    <option value="gene_gi">NCBI Gene GI</option>
						    <option value="nucleotide_gi">NCBI Nucleotide GI</option>
						    <option value="protein_acc">NCBI Protein Accesion No</option>
						    <option value="protein_gi">NCBI Protein GI</option>
						    <option value="protegen_id">Vaximmutordb ID</option>
						    <option value="vo_id">VO ID</option>
						    <option value="other_id">Other Database ID</option>
                          </select>
						    <input name="db_id" type="text" id="db_id" size="30" maxlength="60" /></td>
					  </tr>
    <tr>
      <td bgcolor="#F8FAFA" class="styleLeftColumn">COG category</td>
      <td class="tdData"><select name="cog_cat_id[]" id="cog_cat_id" size="4" multiple="multiple">
			<option value="" selected="selected">Any</option>
<?php 
	foreach ($cog_cats as $cog_cat_id => $cog_cat_label) {
?>
            <option value="<?php echo $cog_cat_id?>"><?php echo $cog_cat_label?></option>
            <?php 
	}
?>
          </select></td>
    </tr>
    <tr>
      <td bgcolor="#F8FAFA" class="styleLeftColumn"> Subcellular Localization</td>
      <td class="tdData"><select name="c_localization[]" id="localization" size="4" multiple="multiple">
        <option value="Any"  selected="selected">Any Localization</option>
        <option value="Cellwall" > Cellwall </option>
        <option value="Cytoplasmic" > Cytoplasmic </option>
        <option value="CytoplasmicMembrane" > Cytoplasmic Membrane </option>
        <option value="Extracellular" > Extracellular </option>
        <option value="OuterMembrane" > Outer Membrane </option>
        <option value="Periplasmic" > Periplasmic </option>
        <option value="Unknown" > Unknown </option>
      </select></td>
    </tr>
    <tr>
      <td bgcolor="#F8FAFA" class="styleLeftColumn">  Maximum Number of Transmembrane Helices</td>
      <td class="tdData"><input name="c_max_tmhmm_PredHel" type="text" id="c_max_tmhmm_PredHel" value="1" />
        <input name="c_max_tmhmm_PredHel_check" type="checkbox" id="c_max_tmhmm_PredHel_check" value="true"  />
        (Note: check box to include) <a href="docs/index.php"></a></td>
    </tr>
    <tr>
      <td bgcolor="#F8FAFA" class="styleLeftColumn"> Minimum Adhesin Probability (0-1.0) </td>
      <td class="tdData"><input name="c_min_spaan_score" type="text" id="c_min_spaan_score" value="0.51" />
        <input name="c_min_spaan_score_check" type="checkbox" id="c_min_spaan_score_check" value="true"  />
        <a href="docs/index.php"></a></td>
    </tr>
    <tr>
      <td bgcolor="#F8FAFA" class="styleLeftColumn"> No Similarity to Human Proteins </td>
      <td class="tdData"><input name="c_human_alignment" type="checkbox" id="c_human_alignment" value="true"  />
        <a href="docs/index.php"></a></td>
    </tr>
    <tr>
      <td bgcolor="#F8FAFA" class="styleLeftColumn"> No Similarity to Mouse Proteins</td>
      <td class="tdData"><input name="c_mouse_alignment" type="checkbox" id="c_mouse_alignment" value="true"  />
        <a href="docs/index.php"></a></td>
    </tr>
						<tr>
							<td class="styleLeftColumn"><strong>Description</strong></td>
							<td class="smallContent"><input name="keywords" type="text" id="keywords" size="60" maxlength="60" />
								(e.g., superoxide dismutase) </td>
						</tr>
						<tr align="center" bgcolor="#ECEFF0">
							<td colspan="2"><input name="submit1" type="submit" class="smallContent" value="  Search  " /></td>
						</tr>
					</table>
				  <p>&nbsp;</p>
					<p><strong class="welcome">Note: </strong>As an example, the Influenza live attenuated vaccine <a href="http://www.violinet.org/vaxquery/vaccine_detail.php?c_vaccine_id=328&amp;keywords=Flumist"><strong>FluMist</strong></a> has over 50 Vaximmutors manually annotated in VaximmutorDB. </p>
				    <p>&nbsp;</p>
                </form>
<h3 class="style2" id="dva"><strong>Statistics:</strong> Vaximmutordb includes
			<?php echo $total_antigen?>
	immune factors for now. </h3>
<p><strong>Provenance: </strong>The data in Vaximmutordb originates from our literature curation and  boinformatics analyses.</p>
<p>&nbsp;</p>
		<!-- InstanceEndEditable --></div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
