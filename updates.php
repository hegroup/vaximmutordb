<?php 
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Protegen: Protective Antigens</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
  <h3 align="center">VaximmutorDB News and Updates</h3>
  <br/>
 <ul>
    <li><strong>2014-2017: </strong>Continuous VaximmutorDB data collection and annotation. </li>
    <li><strong>3/27/2014:</strong> Announced the <a href="http://www.violinet.org/vaximmutordb/index.php">VaximmutorDB</a> project. </li>
    <li><strong>10/30/2012 - 3/27/2014:</strong> Several UROP students worked on this project. </li>
    <li><strong>10/30/2012:</strong> The VaximmutorDB website was set up as an individual program in VIOLIN. Allen developed a script to extract data from the VIOLIN vaccine database. </li>
 </ul>
  <p/>
  <!-- InstanceEndEditable --></div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
