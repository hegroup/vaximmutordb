<?php 
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
  <h3 align="center">VaximmutorDB Introduction</h3>
  <p>Infectious diseases are still a major source of human mortality throughout the world. As one of the most significant inventions in modern medicine, vaccination has been used to dramatically protect humans against many infectious diseases and improve human health. However, our efforts to develop vaccines to protect against many diseases have not been successful. Future success of effective vaccine development relies on deep understanding of the molecular mechanisms of vaccine-host interactions and vaccine-induced immune pathways and networks. </p>
  <p>A successful vaccination induces a series of immune responses eventually leading to an adaptive immunity against an infection or non-infectious disease. However, how exactly the immune responses are conducted is unclear yet. The VaximmutorDB is established with an aim to target this challenge.  VaximmutorDB is a web-based central database and analysis   system that curates, stores, and analyzes vaccine immune factors. </p>
  <p>For easy explanation, here we define the term &quot;vaximmutor&quot; as an immune effector that is   triggered by a vaccination and plays a role in vaccine-induced host   immunity. Vaximmuntors are specifically targeted by innate and acquired immune   response of the host and are able to induce protection in the host   against infectious and non-infectious diseases. Vaximmutors play   important roles in development of vaccines and biological markers for   disease diagnosis and analysis of fundamental host immunity against   diseases.</p>
  <p>VaximmutorDB is a web-based database of “vaximmutors”.  Over 1,700 vaximmutors have been collected, specific expression profiles of these vaximmutors under different experimental conditions and the interaction pathways and networks among these vaximmutors are not yet understood.</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <!-- InstanceEndEditable --></div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
