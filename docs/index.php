<?php 
include_once('../../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
  <h3 align="center">VaximmutorDB Documentation</h3>
  <p>The VaximmutorDB website is user-friendly and easy to use and explore.  </p>
  <p><strong>1. VaximmutorDB data curation</strong></p>
  <p>The vaximmuntor bioinformatics curation was achieved through an internally developed, web-based Limix literature curation and analysis system.  Specifically, the papers annotated came from the PubMed literature database. The annotation of the data in the papers was achieved based on pre-defined database schema (or categories). With a PubMed paper ID or a Gene ID, our system is able to to retrieve all related information about the literature citation or the gene, respectively. The system also allows an annotator to submit data and a reviewer to double check, edit if needed, and approve the submitted annotations. Only after the review and approval by a domain expert, the information is available in the VaximmuntorDB website for users to query and browse. </p>
  <p>See more information on the <a href="http://www.violinet.org/docs/tutorial.php">VIOLIN account registration and data submission tutorial website</a>. </p>
  <p>&nbsp;</p>
  <p><strong>2. Website query</strong></p>
  <p>Querying the VaximmutorDB is straightforward. Figure 1 provides a good example of querying vaximmutor information in the VaximmutorDB web site.  </p>
  <p><img src="query.png" width="867" alt="query" /></p>
  <p><strong>Figure 1. VaximmuntorDB data query. </strong>(A) Search VaximmutorDB for influenza virus vaccines and gene names containing “IFN”. (B) Two genes results found and each gene is associated with two influenza vaccines. (C) The detailed vaccine information and how the gene was regulated in vaccinated hosts are provided in another VaximmutorDB page. (D) A click of the VO ID leads to an <a href="http://www.ontobee.org">Ontobee</a> page showing the information about this vaccine. For example, after clicking, the VO ID (VO_0000044) points to the Ontobee web page: <a href="http://purl.obolibrary.org/obo/VO_0000044">http://purl.obolibrary.org/obo/VO_0000044</a>. </p>
  <p>&nbsp;</p>
  <p><strong>3. BLAST analysis</strong></p>
  <p>The VaximmutorDB BLAST analysis supports the BLAST sequence similarity analysis using a customized BLAST data library based on the data stored in the VaximmutorDB database. </p>
  <p>&nbsp; </p>
  <p>&nbsp;</p>
  <p align="center">&nbsp;</p>
  <!-- InstanceEndEditable --></div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
