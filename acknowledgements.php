<?php 
include_once('../inc/functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
                <h3><strong>Acknowledgements</strong></h3>
                <p>&nbsp;</p>
                <p><strong>The Vaximmutordb project is supported by four outstanding  undergraduate students in Unversity of Michigan: </strong></p>
        <ul>
          <li><strong>Kimberly Berke</strong>, <a href="https://lsa.umich.edu/urop">UROP</a> undergraduate student (Sept 2013 - May 2015), then research assistant (May 2015 - )</li>
          <li><strong>Peter Sun</strong>, UROP undergraduate student (Sept 2014 - May 2015), then research assistant (May 2015 - )</li>
          <li><strong>Rebecca Racz</strong>, Student, Undergraduate, Biology, L S &amp; A, University of Michigan (Time: Sept 2010 - 2015), research assistant</li>
          <li><strong>Monica Ja Young Chung</strong>, Undergraduate, Biology, L S &amp; A, University of Michigan (Time: January 2011  - December 2012), research volunteer</li>
        </ul>
        <p>These students  have been responsible for identification, manual curation, and analysis of various &quot;Vaximmutordb&quot; genes.  </p>
        <p><strong>The webmaster and software developer of the Vaximmutordb program is  Mr. Zuoshuang Xiang (until March 2013), Edison Ong. </strong></p>
                <p><strong>Dr. Oliver He designed and managed the project. Oliver trained the students and was the domain expert who manually reviewed and approved the data curated by students. </strong></p>
                <p><strong>In addition to the <a href="https://lsa.umich.edu/urop">UROP</a> support, the VIOLIN project was also supported by NIH-NIAID grant R01AI081062 to Dr. Yongqun &quot;Oliver&quot;He. </strong></p>
                <p> </p>
                <p></p>
                <p>&nbsp;</p>
  <!-- InstanceEndEditable --></div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
