<?php 
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);

$PROGRAM = isset($_REQUEST['PROGRAM']) ? $_REQUEST['PROGRAM'] : ''; 
$DATALIB = isset($_REQUEST['DATALIB']) ? $_REQUEST['DATALIB'] : ''; 
$INPUT_TYPE = isset($_REQUEST['INPUT_TYPE']) ? $_REQUEST['INPUT_TYPE'] : ''; 
$sequence = isset($_REQUEST['sequence']) ? $_REQUEST['sequence'] : ''; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
<!--
.style2 {font-size: 15px}
-->
</style>
<!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
<?php 
$cog_cats=array();
$strSql = "SELECT * from pathinfo.cog_cat order by cog_cat_label";
$rs = $db->Execute($strSql);
foreach ($rs as $row) {
	$cog_cats[$row['cog_cat_id']] = $row['cog_cat_label'];
}

$organism_types['All'] = 'all';
$organism_types['Bacteria'] = 'bacterium';
$organism_types['Virus'] = 'virus';
$organism_types['Parasite'] = 'parasite';
$organism_types['Fungi'] = 'fungus';
$organism_types['Allergy-Cancer'] = 'allergy_cancer';

/*
foreach ($organism_types as $type_text => $type_value) {
	$type_value="vimmutordb_{$type_value}_a ";
	print ($type_value) ;
}
foreach ($organism_types as $type_text => $type_value) {
	$type_value="vimmutordb_{$type_value}_n ";
	print ($type_value) ;
}
*/
?>

				<h3 align="center"><strong>Vaximmutordb BLAST search for genes and proteins associated with vaccine:</strong></h3>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The DNA and protein sequences of protective antigens stored in Vaximmutordb  can be searched by our costomized BLAST programs. Please provide your  sequence data as input. The output will be protective antigens that  share DNA/protein sequences with your input sequence(s).</p>
				<form action="../blast/blast_cs.cgi" method = "post" enctype= "multipart/form-data" name="MainBlastForm" id="MainBlastForm">
					<table style="border:1px solid #999966; margin-left:20px;">
						<tr>
							<td height="40" bgcolor="#DDDDDD" ><b>Vaximmutordb BLAST for  genes and proteins associated with vaccine: </b></td>
						</tr>

						<tr>
							<td bgcolor="#F8FAFA" class="styleLeftColumn"><a href="../blast/docs/blast_program.html">Program</a>
								<select name = "PROGRAM">
									<option <?php  if ($PROGRAM == 'blastp') { ?> selected="selected"<?php  }?>>blastp</option>
									<option <?php  if ($PROGRAM == 'blastn') { ?> selected="selected"<?php  }?>>blastn</option>
									<option <?php  if ($PROGRAM == 'blastx') { ?> selected="selected"<?php  }?>>blastx</option>
									<option <?php  if ($PROGRAM == 'tblastn') { ?> selected="selected"<?php  }?>>tblastn</option>
									<option <?php  if ($PROGRAM == 'tblastx') { ?> selected="selected"<?php  }?>>tblastx</option>
								</select>
								<a href="../blast/docs/blast_databases.html">Database</a>
								<select name = "DATALIB">
<?php 
foreach ($organism_types as $type_text => $type_value) {
	$type_value="vimmutordb_{$type_value}_a";

?>                                
									<option value = "<?php echo $type_value?>" <?php  if ($DATALIB == $type_value) { ?> selected="selected"<?php  }?>><?php echo $type_text?> Vaximmutordb Protein Sequences</option>
<?php 
	$type_value="vimmutordb_{$type_value}_n";

	if ($type_value!='fungus') {
?>                                
									<option value = "<?php echo $type_value?>" <?php  if ($DATALIB == $type_value) { ?> selected="selected"<?php  }?>><?php echo $type_text?> Vaximmutordb DNA Sequences</option>
<?php 
	}
}
?>
								</select></td>
						</tr>
						<tr>
							<td class="styleLeftColumn">Enter here your input data as
								<select name = "INPUT_TYPE">
									<option> Sequence in FASTA format </option>
									<option <?php  if ($INPUT_TYPE == 'Accession or GI') { ?> selected="selected"<?php  }?>> Accession or GI </option>
								</select>
								<input name="submit22" type="submit" value="Search" />
&nbsp;&nbsp;
<input name="button2" type="button" onclick="MainBlastForm.SEQUENCE.value='';MainBlastForm.QUERY_FROM.value='';MainBlastForm.QUERY_TO.value='';MainBlastForm.SEQUENCE.focus();" value="Clear sequence" /></td>
						</tr>
						<tr>
							<td bgcolor="#F8FAFA" class="styleLeftColumn"><textarea name="SEQUENCE" rows="6" cols="100" style="font-family:'Courier New', Courier, mono"><?php echo $sequence?></textarea></td>
						</tr>
						<tr>
							<td class="styleLeftColumn"><p>Or load it from disk
								<input type="file" name="SEQFILE" />
							</p>
						    <p>Please read about <a href="/BLAST/fasta.html">FASTA</a> format description </p>
                            <p> Set subsequence: From
                              <input type="text" name="QUERY_FROM" value="" size="10" />
                              To
  <input type="text" name="QUERY_TO" value="" size="10" />
                            </p>
                            <hr />
The query sequence is <a href="/BLAST/filtered.html">filtered</a> for low complexity regions by default. <br />
<a href="../blast/docs/newoptions.html#filter">Filter</a>
<input type="checkbox" value="L" name="FILTER" checked="checked" />
Low complexity
<input type="checkbox" value="m" name="FILTER" />
Mask for lookup table only
<p> <a href="../blast/docs/newoptions.html#expect">Expect</a>
  <select name = "EXPECT">
    <option value="0.000001" selected="selected"> 0.000001 </option>
    <option value="0.00001"> 0.00001 </option>
    <option value="0.0001"> 0.0001 </option>
    <option value="0.01"> 0.01 </option>
    <option value="1"> 1 </option>
    <option value="10"> 10 </option>
    <option value="100"> 100 </option>
    <option value="1000"> 1000 </option>
  </select>
  &nbsp;&nbsp; <a href="../blast/docs/matrix_info.html">Matrix</a>
  <select name = "MAT_PARAM">
    <option value = "PAM30	 9	 1"> PAM30 </option>
    <option value = "PAM70	 10	 1"> PAM70 </option>
    <option value = "BLOSUM80	 10	 1"> BLOSUM80 </option>
    <option selected="selected" value = "BLOSUM62	 11	 1"> BLOSUM62 </option>
    <option value = "BLOSUM45	 14	 2"> BLOSUM45 </option>
  </select>
  <input type="checkbox" name="UNGAPPED_ALIGNMENT" value="is_set" />
  Perform ungapped alignment </p>
<p> <a href="../blast/docs/newoptions.html#gencodes">Query Genetic Codes (blastx only) </a>
  <select name = "GENETIC_CODE">
    <option> Standard (1) </option>
    <option> Vertebrate Mitochondrial (2) </option>
    <option> Yeast Mitochondrial (3) </option>
    <option> Mold Mitochondrial; ... (4) </option>
    <option> Invertebrate Mitochondrial (5) </option>
    <option> Ciliate Nuclear; ... (6) </option>
    <option> Echinoderm Mitochondrial (9) </option>
    <option> Euplotid Nuclear (10) </option>
    <option> Bacterial (11) </option>
    <option> Alternative Yeast Nuclear (12) </option>
    <option> Ascidian Mitochondrial (13) </option>
    <option> Flatworm Mitochondrial (14) </option>
    <option> Blepharisma Macronuclear (15) </option>
  </select>
</p>
<p> <a href="../blast/docs/newoptions.html#gencodes">Database Genetic Codes (tblast[nx] only) </a>
  <select name = "DB_GENETIC_CODE">
    <option> Standard (1) </option>
    <option> Vertebrate Mitochondrial (2) </option>
    <option> Yeast Mitochondrial (3) </option>
    <option> Mold Mitochondrial; ... (4) </option>
    <option> Invertebrate Mitochondrial (5) </option>
    <option> Ciliate Nuclear; ... (6) </option>
    <option> Echinoderm Mitochondrial (9) </option>
    <option> Euplotid Nuclear (10) </option>
    <option> Bacterial (11) </option>
    <option> Alternative Yeast Nuclear (12) </option>
    <option> Ascidian Mitochondrial (13) </option>
    <option> Flatworm Mitochondrial (14) </option>
    <option> Blepharisma Macronuclear (15) </option>
  </select>
</p>
<p> <a href="../blast/docs/oof_notation.html">Frame shift penalty</a> for blastx
  <select name = "OOF_ALIGN">
    <option> 6 </option>
    <option> 7 </option>
    <option> 8 </option>
    <option> 9 </option>
    <option> 10 </option>
    <option> 11 </option>
    <option> 12 </option>
    <option> 13 </option>
    <option> 14 </option>
    <option> 15 </option>
    <option> 16 </option>
    <option> 17 </option>
    <option> 18 </option>
    <option> 19 </option>
    <option> 20 </option>
    <option> 25 </option>
    <option> 30 </option>
    <option> 50 </option>
    <option> 1000 </option>
    <option selected="selected" value = "0"> No OOF </option>
  </select>
</p>
<p> Limit search to results of <a href="http://www.ncbi.nlm.nih.gov/entrez/query/static/help/helpdoc.html#Writing_Advanced_Search_Statements">Entrez query</a>
  <input type="text" name = "ENTREZ_QUERY" maxlength="50" />
</p>
<p> <a href="../blast/docs/full_options.html">Other advanced options:</a> &nbsp;&nbsp;&nbsp;&nbsp;
  <input type="text" name="OTHER_ADVANCED" value="" maxlength="50" />
  &nbsp;
  <input type="checkbox" name="TAX_BLAST" />
  Show <a href ="../blast/docs/taxblasthelp.html">Tax Blast reports</a></p>
<hr />
<input type="checkbox" name="NCBI_GI" />
&nbsp;&nbsp; <a href="../blast/docs/newoptions.html#ncbi-gi"> NCBI-gi</a> &nbsp;&nbsp;&nbsp;&nbsp;
<input type="checkbox" name="OVERVIEW"  checked="checked" />
<a href="../blast/docs/newoptions.html#graphical-overview">Graphical Overview</a> &nbsp;&nbsp; <a href="../blast/docs/options.html#alignmentviews">Alignment view</a>
<select name = "ALIGNMENT_VIEW">
  <option value="0"> Pairwise </option>
  <option value="1"> master-slave with identities </option>
  <option value="2"> master-slave without identities </option>
  <option value="3"> flat master-slave with identities </option>
  <option value="4"> flat master-slave without identities </option>
  <option value="7"> BLAST XML </option>
  <option value="8"> Hit Table </option>
</select>
<br />
<a href="../blast/docs/newoptions.html#descriptions">Descriptions</a>
<select name = "DESCRIPTIONS">
  <option> 0 </option>
  <option> 10 </option>
  <option> 50 </option>
  <option selected="selected"> 100 </option>
  <option> 250 </option>
  <option> 500 </option>
</select>
&nbsp;&nbsp; <a href="../blast/docs/newoptions.html#alignments">Alignments</a>
<select name = "ALIGNMENTS">
  <option> 0 </option>
  <option> 10 </option>
  <option selected="selected"> 50 </option>
  <option> 100 </option>
  <option> 250 </option>
  <option> 500 </option>
</select>
<a href="../blast/docs/color_schema.html">Color schema</a>
<select name = "COLOR_SCHEMA">
  <option selected="selected" value = "0"> No color schema </option>
  <option value = "1"> Color schema 1 </option>
  <option value = "2"> Color schema 2 </option>
  <option value = "3"> Color schema 3 </option>
  <option value = "4"> Color schema 4 </option>
  <option value = "5"> Color schema 5 </option>
  <option value = "6"> Color schema 6 </option>
</select>
</td>
						</tr>

						<tr align="center" bgcolor="#ECEFF0">
							<td><input name="button" type="button" onclick="MainBlastForm.SEQUENCE.value='';MainBlastForm.SEQFILE.value='';MainBlastForm.SEQUENCE.focus();" value="Clear sequence" />
								<input name="submit2" type="submit" value="Search" /></td>
						</tr>
					</table>
				</form>
  <!-- InstanceEndEditable -->
</div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
