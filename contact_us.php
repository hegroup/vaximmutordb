<?php 
include("../inc/recaptcha-php-1.10/recaptchalib.php");
include_once('../inc/functions.php');
$db = ADONewConnection($driver);
$db->Connect($host, $username, $password, $database);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>VIOLIN: Vaccine Investigation and Online Information Network</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<link rel="shortcut icon" href="/favicon.ico"/>
<link href="../css/bmain.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/common.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body style="margin:0px;" id="main_body">
<!-- InstanceBeginEditable name="TopBanner" -->
<?php 
include('../inc/template_vaximmutordb_top.php');
?>
<!-- InstanceEndEditable -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="160" valign="top">
<!-- InstanceBeginEditable name="LeftNavBar" -->
<?php 
include('../inc/template_vaximmutordb_left.php');
?>
<!-- InstanceEndEditable -->
  </td>
    <td valign="top">
<?php 
if (isset($_SESSION['c_user_name'])) {
?>
<div style="text-align:right; margin: 2px 8px 2px 8px;">            
<?php 
include('../inc/template_small_navi.php');
?>
</div>
<?php 
}
?>
<div style="margin:6px 10px 16px 16px;">
  <!-- InstanceBeginEditable name="Main" -->
  <h3><strong>Contact Us</strong></h3>
  <p>The Vaximmutordb system has been created and maintained at  University of Michigan Medical School, Ann Arbor, MI. Please contact us: </p>
  <blockquote class="block_generic">
    <blockquote class="block_generic">&nbsp;</blockquote>
  </blockquote>
  <blockquote class="block_generic">
    <p><a href="http://www.hegroup.org/aboutUs/Oliver.html">Yongqun He</a> (PI), DVM, PhD, Associate Professor, Unit for Lab Animal Medicine, Department of Microbiology and Immunology, and Center for Computational Medicine and Biology, University of Michigan Medical School </p>
    <blockquote class="block_generic">Email: yongqunh AT med.umich.edu </blockquote>
    <blockquote class="block_generic">Phone: (734) 615-8231 </blockquote>
  </blockquote>
  <br />
<p><strong>Feedback</strong>    </p>
<form action="../feedback/feedback_submit.php" method="post" name="SubmitFeedbackForm" id="SubmitFeedbackForm">
  <p>Your feedback allows us to enhance the Vaximmutordb system for the best possible user experience. Please take a few minutes to let us know what you think. Thank you!</p>
    <table style="border:1px solid #999966" cellpadding="4" cellspacing="0">
      <tr>
        <td bgcolor="#F4EFEA">Category: </td>
        <td bgcolor="#F4EFEA"><select name="c_category">
            <option value="Query">Query</option>
            <option value="Registration">Registration</option>
            <option value="Suggestion">Suggestion</option>
            <option value="Correction">Correction</option>
            <option value="Errors">Errors</option>
            <option value="Other">Other</option>
          </select></td>
      </tr>
      <tr>
        <td>E-mail:</td>
        <td><input name="c_email" maxlength="100" size="60" value="" type="text" /></td>
      </tr>
      <tr>
        <td bgcolor="#F4EFEA">Re-enter E-mail:</td>
        <td bgcolor="#F4EFEA"><input name="c_email2" maxlength="100" size="60" value="" type="text" /></td>
      </tr>
      <tr>
        <td>Subject:</td>
        <td><input name="c_subject" maxlength="200" size="110" value="" type="text" /></td>
      </tr>
      <tr>
        <td bgcolor="#F4EFEA">Message:</td>
        <td bgcolor="#F4EFEA"><textarea name="c_body" cols="110" rows="4"></textarea></td>
      </tr>
          <tr>
            <td>Verify via reCAPTCHA:</td>
            <td><?php echo recaptcha_get_html(RECAPTCHA_PUBLICKEY);?></td>
          </tr>
      <tr>
        <td colspan="2" align="center" bgcolor="#EEEEEE"><input name="submit" type="submit" value="Submit" />
          <input style="margin-left:40px;" type="button" name="Button" value="Cancel" onclick="window.location.href='/index.php'"/></td>
      </tr>
    </table>
  </form>
  <!-- InstanceEndEditable -->
</div>
    </td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4869243-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
